package com.evoscalabootcamp.error_handling

import java.time.LocalDate

import cats.syntax.all._
import com.evoscalabootcamp.error_handling.ErrorHandling.Homework._
import com.evoscalabootcamp.error_handling.ErrorHandling._
import org.scalacheck.Gen._
import org.scalatest.Assertion
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class ErrorHandlingSpec
  extends AnyFlatSpec
    with Matchers
    with ScalaCheckDrivenPropertyChecks {


  "CardCreator" should "create valid or return set of errors card" in {
    PaymentCardValidator.validate("Anton Antonov", "5332991026814812", "09/22", "333") shouldBe
      PaymentCard("Anton Antonov", 5332991026814812L, LocalDate.of(2022,9,1), securityCode = 333).valid
    }
 }

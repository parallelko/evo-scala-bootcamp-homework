package com.evoscalabootcamp.basics

import scala.io.Source
import scala.util.Try
import cats.implicits._

object ControlStructuresHomework {
  // Homework

  // Create a command line application that reads various "commands" from the
  // stdin, evaluates them, and writes output to stdout.

  // Commands are:

  //   divide 4 5
  // which should output "4 divided by 5 is 0.8"

  //   sum 5 5 6 8.5
  // which should output "the sum of 5 5 6 8.5 is 24.5"

  //   average 4 3 8.5 4
  // which should output "the average of 4 3 8.5 4 is 4.875"

  //   min 4 -3 -17
  // which should output "the minimum of 4 -3 -17 is -17"

  //   max 4 -3 -17
  // which should output "the maximum of 4 -3 -17 is 4"

  // In case of commands that cannot be parsed or calculations that cannot be performed,
  // output a single line starting with "Error: "

  sealed trait Command
  object Command {
    final case class Divide(dividend: Double, divisor: Double) extends Command
    final case class Sum(numbers: List[Double]) extends Command
    final case class Average(numbers: List[Double]) extends Command
    final case class Min(numbers: List[Double]) extends Command
    final case class Max(numbers: List[Double]) extends Command
  }

  final case class ErrorMessage(value: String) {
    def render: String = s"Error: $value"
  }

  sealed trait Result {}
  object Result {
    final case class IntVal(value: Int) extends Result
    final case class DoubleVal(value: Double) extends Result
    final case class DivideRes(value: Double) extends Result
  }

  def safe[A](a: => A): Either[ErrorMessage, A] = Try(a).toEither.leftMap(error => ErrorMessage(error.getMessage))

  def parseCommand(x: String): Either[ErrorMessage, Command] = {
    x.split(" ").toList match {
      case Nil                          => Left(ErrorMessage("Please enter a command"))
      case x::Nil                       => Left(ErrorMessage("Provide arguments"))
      case "divide"::a::b::Nil          => safe(Command.Divide(a.toDouble, b.toDouble))
      case "divide"::_::_::xs           => Left(ErrorMessage("Divide command required only 2 arguments"))
      case "average"::xs                => safe(Command.Average(xs.map(_.toDouble)))
      case "sum"::xs                    => safe(Command.Sum(xs.map(_.toDouble)))
      case "min"::xs                    => safe(Command.Min(xs.map(_.toDouble)))
      case "max"::xs                    => safe(Command.Max(xs.map(_.toDouble)))
      case x                            => Left(ErrorMessage(s"Unknown command${x.head}"))
    }
  }
  // should return an error (using `Left` channel) in case of division by zero and other
  // invalid operations
  def calculate(x: Command): Either[ErrorMessage, Result] = {
    x match {
      case Command.Divide(a,b)      => if (b==0) Left(ErrorMessage("division by zero")) else Right(Result.DivideRes(a/b))
      case Command.Average(numbers) => Right(Result.DoubleVal(numbers.sum/numbers.length))
      case Command.Max(numbers)     => Right(Result.DoubleVal(numbers.max))
      case Command.Min(numbers)     => Right(Result.DoubleVal(numbers.min))
      case Command.Sum(numbers)     => Right(Result.DoubleVal(numbers.sum))
      case _                        => Left(ErrorMessage("Unknown Command in Calculation"))
    }
  }

  def renderResult(x: Result, input: List[String]): String = {
    x match {
      case Result.DivideRes(a) => s"${input.slice(1,3).head} divided by ${input.slice(1,2).tail.head} is $a"
      case Result.IntVal(a) => s"the ${input.head} of ${input.tail.concat(" ")} is $a"
      case Result.DoubleVal(a) => s"the ${input.head} of ${input.tail.concat(" ")} is $a"
    }
  }

  def process(x: String): String = {
    (for {
      command <- parseCommand(x.strip())
      calc <- calculate(command)
      render = renderResult(calc, x.strip().split(" ").toList)
    } yield render).leftMap(_.render).merge
  }

  // This `main` method reads lines from stdin, passes each to `process` and outputs the return value to stdout
  def main(args: Array[String]): Unit = Source.stdin.getLines() map process foreach println
}

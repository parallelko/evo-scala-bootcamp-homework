package com.evoscalabootcamp.basics

object ClassesAndTraits {

  // Homework
  //
  // Add additional 2D shapes such as triangle and square.
  //
  // In addition to the 2D shapes classes, add also 3D shapes classes
  // (origin, point, sphere, cube, cuboid, 3D triangle - you can add
  // others if you think they are a good fit).
  //
  // Add method `area` to 2D shapes.
  //
  // Add methods `surfaceArea` and `volume` to 3D shapes.
  //
  // If some of the implementation involves advanced math, it is OK
  // to skip it (leave unimplemented), the primary intent of this
  // exercise is modelling using case classes and traits, and not math.


  // 2D Classes
  sealed trait Shape extends Located with Movable with Bounded with Area2D

  sealed trait Located {
    def x: Double
    def y: Double
  }

  sealed trait Bounded {
    def minX: Double
    def maxX: Double
    def minY: Double
    def maxY: Double
  }

  sealed trait Movable {
    def move(dx: Double, dy: Double): Shape
  }

  sealed trait Area2D {
    def area: Double
  }

  final case class Point(X:Double, Y:Double) extends Shape {
    override def x: Double = X
    override def y: Double = Y
    override def area: Double = 0
    override def minX: Double = x
    override def minY: Double = y
    override def maxX: Double = x
    override def maxY: Double = y
    // I got error in IDEA that i can't use move methods in Triangle class in case when move defined in trait by default
    override def move(dx: Double, dy: Double): Point = Point(x+dx, y+dy)
  }

  final case class Triangle(p1: Point, p2: Point, p3:Point) extends Shape {
    override def x: Double = p1.x
    override def y: Double = p1.y
    override def minX: Double = List(p1.x, p2.x, p3.x).min
    override def maxX: Double = List(p1.x, p2.x, p3.x).max
    override def minY: Double = List(p1.y, p2.y, p3.y).min
    override def maxY: Double = List(p1.y, p2.y, p3.y).max
    override def area: Double = ((p1.x-p3.x)*(p2.y-p3.y)-(p2.x-p3.x)*(p1.y-p3.y))/2

    // I got error in IDEA that i can't use Point.move method when define new Triange in case of move method defined directly in trait definition
    override def move(dx: Double, dy: Double): Triangle = Triangle(p1.move(dx,dy), p2.move(dx, dy), p3.move(dx, dy))
  }

  case class Square(p1: Point, size: Double) extends Shape {
    override def x: Double = p1.x
    override def y: Double = p1.y
    override def minX: Double = p1.x min p1.x + size //in Case of negative values of Size
    override def maxX: Double = p1.x max p1.x + size
    override def minY: Double = p1.y min p1.y + size
    override def maxY: Double = p1.y max p1.y + size
    override def area: Double = Math.pow(size, 2)

    override def move(dx: Double, dy: Double): Square = Square(p1.move(dx,dy), size)
  }

  // 3D Classes
  sealed trait Located3D extends Located {
    def z: Double
  }

  sealed trait Bounded3D extends Bounded {
    def minZ: Double
    def maxZ: Double
  }

  sealed trait Movable3D {
    def move(dx: Double, dy: Double, dz: Double): Shape3D
  }

  sealed trait Area3D {
    def surfaceArea: Double
  }

  sealed trait Volumes {
    def volume: Double
  }

  sealed trait Shape3D extends Located3D with Movable3D with Bounded3D with Area3D with Volumes

  final case class Point3D(override val x: Double, override val y: Double, z: Double) extends Shape3D {
    override def maxZ: Double = z
    override def minZ: Double = z
    override def surfaceArea: Double = 0
    override def volume: Double = 0
    override def move(dx: Double, dy: Double, dz: Double): Point3D = Point3D(x+dx, y+dy, z+dz)

    override def minX: Double = x
    override def maxX: Double = x
    override def minY: Double = y
    override def maxY: Double = y
  }

  final case class Vector3D(start: Point3D, end:Point3D) extends Shape3D {
    override def x: Double = end.x
    override def y: Double = end.y
    override def z: Double = end.z
    override def maxX: Double = start.x max end.x
    override def minX: Double = start.x min end.x
    override def maxY: Double = start.y max end.y
    override def minY: Double = start.y min end.y
    override def maxZ: Double = start.z max end.z
    override def minZ: Double = start.z min end.z
    override def surfaceArea: Double = 0
    override def volume: Double = 0
    override def move(dx: Double, dy: Double, dz: Double): Vector3D = Vector3D(start.move(dx, dy, dz), end.move(dx,dy,dz))
    def deltaX: Double = end.x - start.x
    def deltaY: Double = end.y - start.y
    def deltaZ: Double = end.z - start.z
  }


  final case class Sphere(p:Point3D, r: Double) extends Shape3D {
    override def x: Double = p.x
    override def y: Double = p.y
    override def z: Double = p.z
    override def maxX: Double = x+r
    override def minX: Double = x-r
    override def maxY: Double = y+r
    override def minY: Double = y-r
    override def maxZ: Double = z+r
    override def minZ: Double = z-r
    override def surfaceArea: Double = 4 * Math.PI * Math.pow(r, 2)
    override def move(dx: Double, dy: Double, dz: Double): Sphere = Sphere(p.move(dx,dy,dz), r)
    override def volume: Double = 4 * Math.PI * Math.pow(r, 3) / 3
  }

  final case class Cube(p:Point3D, size: Double) extends Shape3D {
    override def x: Double = p.x
    override def y: Double = p.y
    override def z: Double = p.z
    override def maxX: Double = x+size/2
    override def minX: Double = x-size/2
    override def maxY: Double = y+size/2
    override def minY: Double = y-size/2
    override def maxZ: Double = z+size/2
    override def minZ: Double = z-size/2
    override def surfaceArea: Double = 6 * Math.pow(size, 2)
    override def move(dx: Double, dy: Double, dz: Double): Cube = Cube(p.move(dx,dy,dz), size)
    override def volume: Double = Math.pow(size, 3)
  }

  final case class Cuboid(vect: Vector3D) extends Shape3D {
    override def x: Double = vect.x
    override def y: Double = vect.y
    override def z: Double = vect.z
    override def maxX: Double = vect.maxX
    override def minX: Double = vect.minX
    override def maxY: Double = vect.maxY
    override def minY: Double = vect.minY
    override def maxZ: Double = vect.maxZ
    override def minZ: Double = vect.minZ
    override def surfaceArea: Double = 2 * (vect.deltaX*vect.deltaY + vect.deltaX*vect.deltaZ + vect.deltaY*vect.deltaZ)
    override def move(dx: Double, dy: Double, dz: Double): Cuboid = Cuboid(vect.move(dx, dy, dz))
    override def volume: Double = vect.deltaX*vect.deltaY*vect.deltaZ
  }

  final case class Triangle3D(p1: Point3D, p2: Point3D, p3: Point3D, p4: Point3D) extends Shape3D {
    override def x: Double = p1.x
    override def y: Double = p1.y
    override def z: Double = p1.z
    override def minX: Double = List(p1.x, p2.x, p3.x, p4.x).min
    override def maxX: Double = List(p1.x, p2.x, p3.x, p4.x).max
    override def minY: Double = List(p1.y, p2.y, p3.y, p4.y).min
    override def maxY: Double = List(p1.y, p2.y, p3.z, p4.z).max
    override def minZ: Double = List(p1.z, p2.z, p3.z, p4.z).min
    override def maxZ: Double = List(p1.z, p2.z, p3.z, p4.z).max

    //It could be found by founding area of Triangles class after switching coordinate systems
    override def surfaceArea: Double = ???

    override def move(dx: Double, dy: Double, dz: Double): Triangle3D = Triangle3D(p1.move(dx, dy, dz), p2.move(dx, dy, dz), p3.move(dx, dy, dz), p4.move(dx, dy, dz))
    override def volume: Double = ???
  }
}
package com.evoscalabootcamp.basics

import scala.annotation.tailrec

object DataStructures {

  // Homework
  //
  // Implement a special sort which sorts the keys of a map (K) according to their associated
  // values (V).
  //
  // In case of "ties" (equal values) it should group these keys K into Set-s in the results.
  //
  // The input is a map where keys (K) are the values to be sorted and values are their associated numeric
  // values.
  //
  // The output is a list (in ascending order according to the associated `Int` values) of tuples of `Set`-s
  // with values from K, and the associated value V for these values in the `Set`.
  //
  // For example:
  //
  // Input `Map("a" -> 1, "b" -> 2, "c" -> 4, "d" -> 1, "e" -> 0, "f" -> 2, "g" -> 2)` should result in
  // output `List(Set("e") -> 0, Set("a", "d") -> 1, Set("b", "f", "g") -> 2, Set("c") -> 4)`.

  // Implementation without recursion
  def findSameByInt[T](map: Map[T, Int]): Int => (Set[T], Int) = x => (map.filter(el => el._2 == x).keySet, x)

  def sortConsideringEqualValues[T](map: Map[T, Int]): List[(Set[T], Int)] = map.values.toSet.toList.sorted.map(findSameByInt(map))

  // Recursion Implementation
  def mapFilterWithMax[T](map: Map[T, Int]): ((T, Int)) => Boolean = el => el._2 == map.values.max

  def mapFilterWithoutMax[T](map: Map[T, Int]): ((T, Int)) => Boolean = el => el._2 != map.values.max

  // @tailrec tail rec optimization is not work in that case, I don't know why
  def sortConsideringEqualValues2[T](map: Map[T, Int]): List[(Set[T], Int)] =
    if (map.isEmpty) Nil
    else sortConsideringEqualValues2(map.filter(mapFilterWithoutMax(map))).concat(List((map.filter(mapFilterWithMax(map)).keySet, map.filter(mapFilterWithMax(map)).values.head)))

  // Implementation After few advices :)
  def sortConsideringEqualValues3[T](map: Map[T, Int]): List[(Set[T], Int)] = map.foldLeft(Map.empty[Int, Set[T]]) {
    case (acc, (k, v)) => acc + (v -> acc.getOrElse(v, Set.empty + k))
  }.iterator.map {
    case (k, v) => v -> k
  }.toList.sortBy(_._2)

}






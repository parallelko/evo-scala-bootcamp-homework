package com.evoscalabootcamp.basics

import scala.annotation.tailrec

object Basics {

  //LazyList implementation
  def makeLazyList(n: Int, base:Int): LazyList[Int] = LazyList.cons(n*base, makeLazyList(n+1, base))
  def isDivide(a: Int): Int => Boolean = _ % a == 0
  def lcm(a: Int, b: Int): Int = {
    val minVal = a min b
    val maxVal = a max b
    makeLazyList(1, maxVal).find(isDivide(minVal)).getOrElse(0)
  }

  //Recursive implementation
  @tailrec
  def recursiveFind(base:Int, n: Int, divider:Int): Int = if (base*n % divider==0) base*n else recursiveFind(base, n+1, divider)
  def lcm2(a:Int, b: Int): Int = {
    recursiveFind(a max b, 1, a min b)
  }

  // Euclidean Algorithm
  @tailrec
  def gcd(a: Int, b: Int): Int = b match {
    case 0 => a
    case _ => gcd(b, b%a)
  }

  def main(args: Array[String]): Unit = {
  }
}

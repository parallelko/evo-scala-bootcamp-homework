package com.evoscalabootcamp.adt

import scala.io.Source
import cats.implicits._

object AlgebraicDataTypes {

  // Homework. Define all algebraic data types, which would be needed to implement “Hold’em Hand Strength”
  // task you completed to join the bootcamp. Use your best judgement about particular data types to include
  // in the solution, you can model concepts like:
  //
  // 1. Suit
  // 2. Rank
  // 3. Card
  // 4. Hand (Texas or Omaha)
  // 5. Board
  // 6. Poker Combination (High Card, Pair, etc.)
  // 7. Test Case (Board & Hands to rank)
  // 8. Test Result (Hands ranked in a particular order for a particular Board, accounting for splits)
  //
  // Make sure the defined model protects against invalid data. Use value classes and smart constructors as
  // appropriate. Place the solution under `adt` package in your homework repository.

  sealed trait Suit {
    def symbol: String
  }
  object Suits {
    case object Hearts extends Suit {
      override def symbol: String = "h"
    }
    case object Diamonds extends Suit {
      override def symbol: String = "d"
    }
    case object Clubs extends Suit {
      override def symbol: String = "c"
    }
    case object Spades extends Suit {
      override def symbol: String = "s"
    }
    private val SuitList = Set(Hearts, Diamonds, Clubs, Spades)
    def fromString(char: String): Option[Suit] = SuitList.find(_.symbol == char)
  }


  //final case class Suit private (value: Char) extends AnyVal
  //object Suit {
  //  def get(value: Char): Option[Suit] = Set('c', 'd', 'h', 's').find(_ == value).map(Suit(_))
  //}

  final case class Rank private (value: Int) extends AnyVal
  object Rank {
    def get(value: Int): Option[Rank] = (2 to 14).find(_ == value).map(Rank(_))
  }

  final case class Card(rank: Rank, suit: Suit)

  final case class Hand private (cards: List[Card])
  object Hand {
    def fromCards(cards: List[Card]): Option[Hand] = if ((cards.length == 2) && !cards.contains(None)) Some(Hand(cards)) else None
  }


  final case class Board private (cards: List[Card])
  object Board {
    def fromCards(cards: List[Card]): Option[Board] = if ((cards.length == 5) && !cards.contains(None)) Some(Board(cards)) else None
  }

  sealed trait Combinations
  object Combinations {
    final case class HighCard(power: Int) extends Combinations
    final case class Pair(power: Int) extends Combinations
    final case class TwoPairs(power: Int) extends Combinations
    final case class ThreeOfAKind(power: Int) extends Combinations
    final case class Straight(power: Int) extends Combinations
    final case class Flush(power: Int) extends Combinations
    final case class FullHouse(power: Int) extends Combinations
    final case class FourOfAKind(power: Int) extends Combinations
    final case class StraightFlush(power: Int) extends Combinations
  }

  sealed trait Game
  final case class TexasHoldem private (hands: List[Hand] , board: Board) extends Game
  object TexasHoldem {
    def create(hands: List[Hand] , board: Board): Option[TexasHoldem] = if (hands.nonEmpty) Some(TexasHoldem(hands, board)) else None
  }

  final case class ErrorMessage(value: String) {
    def render :String = s"Error: $value"
  }

  def calculate(game: Game): Either[ErrorMessage, Map[Hand, Combinations]] = ??? //Method for implementing Logic

  def parseInput(x: String): Either[ErrorMessage, Game] = ??? //Parsing input

  def processGame(x: String): String = {
    (for {
      game <- parseInput(x.strip())
      calc = calculate(game)
    } yield calc.toString).leftMap(_.render).merge
  }

  def main(args: Array[String]): Unit = Source.stdin.getLines() map processGame foreach println
}

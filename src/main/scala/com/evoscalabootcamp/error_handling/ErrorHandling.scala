package com.evoscalabootcamp.error_handling

import scala.concurrent.Future
import scala.util.Try
import scala.util.control.NonFatal
import java.time.LocalDate

import cats.data.ValidatedNec
import cats.syntax.all._

object ErrorHandling extends App {


  def parseLongOption(string: String): Option[Long] =
    Try {
      string.toLong
    }.toOption

  // Homework. Place the solution under `error_handling` package in your homework repository.
  //
  // 1. Model `PaymentCard` class as an ADT (protect against invalid data as much as it makes sense).
  // 2. Add `ValidationError` cases (at least 5, may be more).
  // 3. Implement `validate` method to construct `PaymentCard` instance from the supplied raw data.
  object Homework {

    case class PaymentCard(
                            cardHolderName: String,
                            cardNumber: Long,
                            expirationDate: LocalDate,
                            securityCode: Int
                          )

    sealed trait ValidationError

    object ValidationError {

      final case object NumberIsNotNumeric extends ValidationError {
        override def toString: String = "Card Number should consist of Numbers"
      }

      final case object NumberIsNotFull extends ValidationError {
        override def toString: String = "Card Number should consist of 16 Digits"
      }

      final case object CardNumberIsNotExists extends ValidationError {
        override def toString: String = "This Card Number is not exists"
      }

      final case object UsernameLengthIsInvalid extends ValidationError {
        override def toString: String = "Invalid Cardholder name"
      }

      final case object UsernameHasSpecialCharacters extends ValidationError {
        override def toString: String = "Cardholder name cannot contain special characters"
      }

      final case object ExpirationDateLengthIsInvalid extends ValidationError {
        override def toString: String = "Expiration date should consist of 5 characters"
      }

      final case object ExpirationDateContentIsInvalid extends ValidationError {
        override def toString: String = "Expiration date should be 4 digits devided by \"/\""
      }

      final case object ExpirationDateParseError extends ValidationError {
        override def toString: String = "Expiration date couldn't be parsed"
      }

      final case object ExpirationNumberProvidesInvalid extends ValidationError {
        override def toString: String = "Please provide"
      }

      final case object ExpirationNumberMonthinvalid extends ValidationError {
        override def toString: String = "Month should be in range of 1..12"
      }

      final case object ExpirationDateExpiredError extends ValidationError {
        override def toString: String = "Card is expired"
      }

      final case object SecurityCodeLengthInvalid extends ValidationError {
        override def toString: String = "Security code length is Invalid"
      }

      final case object SecurityCodeIsNotNumeric extends ValidationError {
        override def toString: String = "Security code should consist of Numbers"
      }

      //final case object NumberIsNotNumeric extends ValidationError
      //final case object NumberIsNotNumeric extends ValidationError
      //final case object NumberIsNotNumeric extends ValidationError
      //final case object NumberIsNotNumeric extends ValidationError
      //final case object NumberIsNotNumeric extends ValidationError
      //final case object NumberIsNotNumeric extends ValidationError

      ??? // Add errors as needed
    }


    object PaymentCardValidator {

      import ValidationError._

      type AllErrorsOr[A] = ValidatedNec[ValidationError, A]

      private def validateCardNumber(number: String): AllErrorsOr[Long] = {

        def validateCardNumberToInt: AllErrorsOr[Long] =
          parseLongOption(number.replace(" ", "")).fold[AllErrorsOr[Long]](NumberIsNotNumeric.invalidNec)(_.validNec)


        def validateCardNumberlength(number: Long): AllErrorsOr[Long] =
          if (number.toString.length == 16)
            number.validNec
          else NumberIsNotFull.invalidNec

        def validateCardNumberLuhnAlgorithm(number: Long): AllErrorsOr[Long] = {
          val numberString = number.toString
          val checkingDigits = numberString.take(numberString.length - 1)
          val modifiedlistOfDigit = for {
            (digit, number) <- checkingDigits.toArray.zipWithIndex
            result = number match {
              case x if x % 2 == 0 => (digit.toString.toInt * 2).toString.map(_.toString.toInt).sum
              case _ => digit.toString.toInt
            }
          } yield result

          if ((modifiedlistOfDigit.sum * 9).toString.last == numberString.last)
            number.validNec
          else
            CardNumberIsNotExists.invalidNec
        }

        validateCardNumberToInt andThen validateCardNumberlength andThen validateCardNumberLuhnAlgorithm
      }

      private def validateName(name: String): AllErrorsOr[String] = {

        def validateUsernameLength: AllErrorsOr[String] =
          if (name.length >= 3 && name.length <= 30) name.validNec
          else UsernameLengthIsInvalid.invalidNec

        def validateUsernameContents: AllErrorsOr[String] =
          if (name.matches("^[a-zA-Z ]+$")) name.validNec
          else UsernameHasSpecialCharacters.invalidNec


        validateUsernameLength *> validateUsernameContents
      }


      private def validateExpirationDate(date: String): AllErrorsOr[LocalDate] = {

        def validateProvidedLength: AllErrorsOr[String] =
          if (date.length == 5) date.validNec else ExpirationDateLengthIsInvalid.invalidNec

        def validateContent: AllErrorsOr[String] =
          if (date.matches("^[0-9]{2}/[0-9]{2}$")) date.validNec else ExpirationDateParseError.invalidNec

        def validateProvidedDates(dateString: String): AllErrorsOr[LocalDate] = {
          val dateArray = dateString.split("/").map(_.toInt)

          def validateMonth: AllErrorsOr[Array[Int]] =
            if (dateArray.head > 0 & dateArray.head <= 12) dateArray.validNec
            else ExpirationNumberMonthinvalid.invalidNec

          def validateDate(dateArray: Array[Int]): AllErrorsOr[LocalDate] = {
            val exiprationDate = LocalDate.of(dateArray.tail.head + 2000, dateArray.head, 1)
            if (exiprationDate.isAfter(LocalDate.now()))
              exiprationDate.validNec
            else ExpirationDateExpiredError.invalidNec
          }

          validateMonth andThen validateDate
        }

        validateProvidedLength *> validateContent andThen validateProvidedDates
      }

      private def validateSecurityCode(securityCode: String): AllErrorsOr[Int] = {
        def validateProvidedLength: AllErrorsOr[String] =
          if (securityCode.length == 3) securityCode.validNec else SecurityCodeLengthInvalid.invalidNec

        def validateContent(securityCode: String): AllErrorsOr[Int] = Try {
          securityCode.toInt
        }.toOption.fold[AllErrorsOr[Int]](SecurityCodeIsNotNumeric.invalidNec)(_.validNec)

        validateProvidedLength andThen validateContent
      }

      def validate(
                    name: String,
                    number: String,
                    expirationDate: String,
                    securityCode: String,
                  ): AllErrorsOr[PaymentCard] =
        (validateName(name), validateCardNumber(number), validateExpirationDate(expirationDate), validateSecurityCode(securityCode)).mapN(PaymentCard)
    }

  }

  // Attributions and useful links:
  // https://www.lihaoyi.com/post/StrategicScalaStylePrincipleofLeastPower.html#error-handling
  // https://www.geeksforgeeks.org/scala-exception-handling/
  // https://typelevel.org/cats/datatypes/validated.html
  // https://blog.ssanj.net/posts/2019-08-18-using-validated-for-error-accumulation-in-scala-with-cats.html
}
